# Multi-Instance Learning Visualization

The purpose of this project is to propose a form of visualization for data sets of multiple instances (MI). For this, a series of steps have been created that allow loading a data set and transforming it using known reduction methods to finally obtain a representation of the data set in two graphs; a graph and a radar diagram.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

It is required to have [Python 3](https://www.python.org/) installed in the operative system, [pip](https://pypi.org/project/pip/) package is required too.
In addition, is recommendable to install [virtualenv](https://virtualenv.pypa.io/en/latest/) & [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) for have all the local environment isolate.

### Installing

First Clone the repositorie

```
git clone git@bitbucket.org:jeliecerv/mil_visualization.git
```

When the code is in local into the folder of code and activate the virtualenv

```
cd mil_visualization
workon mil_visualization
```

run the following comand to install all requirements module for the project

```
pip install -r requirements.txt
```

The databse used in this projects is sqlite but if you want change it go to settings and update the [database connection](https://docs.djangoproject.com/en/2.2/ref/settings/#databases).

The settings file is in the path `mil_visualization/settings.py`

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
```
After the database configuration proceed to run the migration.

```
python manage.py migrate
```

To finalize, run the local project

```
python manage.py runserver
```

The command should show the following input

```
System check identified no issues (0 silenced).
December 21, 2019 - 16:06:00
Django version 2.2.4, using settings 'mil_visualization.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

And if all is work fine, go to web browser to address http://127.0.0.1:8000/

## Authors

* **Jorge Valencia**

## Acknowledgments

* Multi-instance Learning (MIL)
* Python code
* Visualizaion technique

