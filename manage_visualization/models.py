from django.db import models


class Reduction(models.Model):
    KPCA = 'kpca'
    FAST_ICA = 'fast_ica'
    ISOMAP = 'isomap'
    LLE = 'lle'
    MDS = 'mds'
    TSNE = 'tsne'
    REDUCTION_METHODS = [
        (KPCA, 'KPCA'),
        (FAST_ICA, 'FastICA'),
        (ISOMAP, 'Isomap'),
        (LLE, 'Locally Linear Embedding'),
        (MDS, 'MDS'),
        (TSNE, 'TSNE')
    ]
    reduction_method = models.CharField(
        max_length=25,
        choices=REDUCTION_METHODS,
        default=KPCA,
    )

    def __str__(self):
        return self.reduction_method


class Algorithm(models.Model):
    algorithm_name = models.SlugField(max_length=255)
    slug = models.SlugField(unique=True, max_length=255)
    reduction = models.ForeignKey(
        Reduction,
        on_delete=models.CASCADE,
        related_name="algorithms",
    )

    def __str__(self):
        return self.algorithm_name
