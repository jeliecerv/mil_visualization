import numpy as np
import pandas as pd
from django.conf import settings
from scipy.io import loadmat
from sklearn import preprocessing


def load_mil_data(data_file):
    # Load data
    if data_file.lower().endswith('.csv'):
        df = pd.read_csv(data_file)
        return df
    elif data_file.lower().endswith('.svm'):
        df = pd.read_csv(data_file, skiprows=0)
        df = pd.DataFrame(df[df.columns[0]].str.split(' ').tolist())

        # Preparation data
        df_raw = pd.DataFrame()
        count = 0
        for col in df.columns:
            if col == 0:
                _, df_raw['bag_id'], df_raw['y'] = df[col].str.split(':').str
            else:
                count += 1
                _, df_raw['feature-%s' % count] = df[col].str.split(':').str
                df_raw['feature-%s' % count] = pd.to_numeric(df_raw['feature-%s' % count],
                                                             errors='coerce')

        return df_raw
    else:
        raise Exception('data file is wrong: {}'.format(data_file))


def load_data_mat(data='musk1_scaled'):

    if data == 'musk1_scaled':
        filename_bag = f'{settings.MEDIA_ROOT}/data/musk1_scaled/Bag2_mus_escal.mat'
        filename_labels = f'{settings.MEDIA_ROOT}/data/musk1_scaled/bagI_mus_escal.mat'
        X_g = loadmat(
            f'{settings.MEDIA_ROOT}/data/musk1_scaled/X_mus_escal.mat')
    elif data == 'data_gauss':
        filename_bag = f'{settings.MEDIA_ROOT}/data/gauss_data/bag_g.mat'
        filename_labels = f'{settings.MEDIA_ROOT}/data/gauss_data/bagI_gauss.mat'
        X_g = loadmat(f'{settings.MEDIA_ROOT}/data/gauss_data/X_g.mat')
    elif data == 'custom_data_gauss':
        return generate_gauss_dataset()
    elif data == 'musk1_original':
        filename_bag = f'{settings.MEDIA_ROOT}/data/musk1_unscaled/Bag2_musk_original.mat'
        filename_labels = f'{settings.MEDIA_ROOT}/data/musk1_unscaled/bagI_musk1_original.mat'
        X_g = loadmat(
            f'{settings.MEDIA_ROOT}/data/musk1_unscaled/X_musk_original.mat')
    else:
        file = data
        filename_bag = f'{settings.MEDIA_ROOT}/data/'+file+'/Bag2.mat'
        filename_labels = f'{settings.MEDIA_ROOT}/data/'+file+'/BagI.mat'
        X_g = loadmat(f'{settings.MEDIA_ROOT}/data/'+file+'/X.mat')

    bag_g = loadmat(filename_bag)
    labels = loadmat(filename_labels)
    try:
        Bag = bag_g['Bag2']
    except KeyError:
        Bag = bag_g['Bag']
    labels = labels['bagI']
    X = X_g['X']
    Bag = np.squeeze(Bag-1)
    Bags = pd.DataFrame(Bag, columns=['bag_id'])
    labels = pd.DataFrame(labels, columns=['y'])
    labels_raw = []
    for index, row in Bags.iterrows():
        labels_raw.append('-1' if labels.y[row['bag_id']] == 0 else '1')

    Bags = Bags.astype(str)  # transform int to str
    labels_by_bag = pd.DataFrame(labels_raw, columns=['y'])
    attr_name = ['feature-%s' % x for x in range(np.size(X, 1))]
    X = pd.DataFrame(X, columns=attr_name)

    df_result = pd.concat((Bags, labels_by_bag, X), axis=1)
    return df_result


def get_data_by_bag(df, bag_id):
    list_drop = ['bag_id', 'y']
    df_bag = pd.DataFrame(df.loc[df['bag_id'] == str(bag_id)])
    y = df_bag.y
    x = df_bag.drop(list_drop, axis=1)
    return x, y


def get_data(df):
    y = df.y
    bags = df.bag_id
    list_drop = ['bag_id', 'y']
    X = df.drop(list_drop, axis=1)

    return X, y, bags


def data_treatment(df_main, range=(0.01, 100)):
    df_main.loc[:, 'y'] = np.where(
        (df_main.loc[:, 'y'] == '-1') | (df_main.loc[:, 'y'] == 'negative'), 'negative', 'positive')
    columns = list(df_main)
    columns.remove('bag_id')
    columns.remove('y')

    min_max_scaler = preprocessing.MinMaxScaler(feature_range=range)
    X, y, bags = get_data(df_main)
    X_minmax = min_max_scaler.fit_transform(X)
    df_result = pd.concat((bags.reset_index(drop=True),
                           y.reset_index(drop=True),
                           pd.DataFrame(X_minmax,
                                        columns=columns).reset_index(drop=True)), axis=1)
    return df_result


def get_data_info(df):
    X, y, bags = get_data(df)
    positive_bags = len(df[df.y == 'positive'].bag_id.unique())
    negative_bags = len(df[df.y == 'negative'].bag_id.unique())
    positive_instances = len(df[df.y == 'positive'].bag_id)
    negative_instances = len(df[df.y == 'negative'].bag_id)

    data_info = {
        'attributes': len(list(X.columns)),
        'positive_bags': positive_bags,
        'negative_bags': negative_bags,
        'positive_instances': positive_instances,
        'negative_instances': negative_instances,
        'total_bags': positive_bags + negative_bags,
        'total_instances': positive_instances + negative_instances
    }

    return data_info


def get_negative_info(df):
    df_negative = df.loc[df['y'] == 'negative']
    X_negative, y_negative, bags_negative = get_data(df_negative)
    return X_negative, y_negative, bags_negative


def generate_gauss_dataset(bags=20, instances_by_bag=3, features=3):
    # Prueba con matriz de covarianza
    # TODO: Maual de usuario y posibles errores--- (crear diagrama de flujo)
    columns_name = ['feature-%s' % x for x in range(0, features)]
    mu, sigma = 0, 1  # mean and standard deviation
    s_positive = np.random.normal(
        mu, sigma, (round((bags * instances_by_bag) / 2), features))

    mu, sigma = 3, 1  # mean and standard deviation
    s_negative = np.random.normal(
        mu, sigma, (round((bags * instances_by_bag) / 2), features))

    s = np.concatenate((s_positive, s_negative), axis=0)
    df_gauss = pd.DataFrame(s, columns=columns_name)

    bags_id_labels = []
    for bag in range(0, bags):
        y = '1' if bag * \
            instances_by_bag < (bags * instances_by_bag) / 2 else '-1'
        for instance in range(instances_by_bag):
            bags_id_labels.append({
                'bag_id': str(bag),
                'y': y
            })
    df_gauss_labels = pd.DataFrame(bags_id_labels)
    df_gauss = pd.concat((df_gauss_labels.reset_index(drop=True),
                          df_gauss.reset_index(drop=True)), axis=1)
    return df_gauss


def clear_session_vars(request):
    request.session.flush()
