# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import plotly.graph_objs as go
from django_plotly_dash import DjangoDash

from manage_visualization.utils.utils import data_treatment

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']


def distribtion_data(df_main, attr):
    app = DjangoDash(
        'distribtion_data', external_stylesheets=external_stylesheets)

    colors = ['rgb(165,0,38)', 'rgb(49,54,149)']
    fig = px.histogram(df_main, x=attr, y="y", color="y",
                       marginal="box",  # or violin, rug
                       hover_data=df_main.columns,
                       color_discrete_sequence=colors)

    fig.update_layout(
        legend=go.layout.Legend(
            x=0,
            y=1.15,
            traceorder="normal",
            font=dict(
                family="sans-serif",
                size=12,
                color="black"
            ),
            bgcolor="LightSteelBlue",
            bordercolor="Black",
            borderwidth=2
        )
    )

    app.layout = html.Div(children=[
        dcc.Loading(
            id="loading-3", children=[dcc.Graph(
                id='example-graph',
                figure=fig,
                config={
                    'displayModeBar': False
                }
            )], type="cube"),

    ])
