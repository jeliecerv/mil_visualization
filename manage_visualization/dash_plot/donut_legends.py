# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import plotly.graph_objs as go
from django_plotly_dash import DjangoDash

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']


def donut_chart(labels, values, colors=['rgb(165,0,38)', 'rgb(49,54,149)']):
    app = DjangoDash(
        'donut_chart', external_stylesheets=external_stylesheets)

    # Use `hole` to create a donut-like pie chart
    fig = go.Figure(
        data=[
            go.Pie(
                labels=labels,
                values=values,
                marker=dict(colors=colors),
                hole=0.8,
                textinfo="label+percent",
                textposition="auto"
            )
        ]
    )

    fig.update_traces(
        hole=.8,
        hoverinfo="label+value"
    )

    fig.update_layout(
        # height=310,
        showlegend=False
    )

    app.layout = html.Div(children=[
        dcc.Graph(
            id='donut-chart',
            figure=fig,
            config={
                'displayModeBar': False
            }
        )
    ])
