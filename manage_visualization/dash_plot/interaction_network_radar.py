import dash
import dash_core_components as dcc
import dash_html_components as html
import networkx as nx
import pandas as pd
import plotly.express as px
import plotly.graph_objs as go
from django_plotly_dash import DjangoDash
from plotly.subplots import make_subplots
from scipy.spatial.distance import pdist, squareform

from manage_visualization.utils.utils import data_treatment, get_data

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']


LAYAOUTS = {
    'spring_layout': nx.layout.spring_layout,
    'kamada_kawai_layout': nx.layout.kamada_kawai_layout,
    'circular_layout': nx.layout.circular_layout,
    'planar_layout': nx.layout.planar_layout,
    'random_layout': nx.layout.random_layout,
    'spectral_layout': nx.layout.spectral_layout

}


def network_radar(df_main):
    app = DjangoDash(
        'network_radar', external_stylesheets=external_stylesheets)
    X, y, bags = get_data(df_main)
    y_size = y.size - 1
    bags_ids = bags.unique()

    # df_main
    df_t = data_treatment(df_main, range=(0.00, 1.00))

    availables_layouts = ['spring_layout', 'kamada_kawai_layout',
                          'circular_layout', 'random_layout', 'planar_layout', 'spectral_layout']
    availables_distance = ['mean', 'max', 'min']
    availables_metric = ['braycurtis', 'canberra', 'chebyshev', 'cityblock',
                         'correlation', 'cosine', 'dice', 'euclidean',
                         'hamming', 'jaccard',
                         'kulsinski', 'mahalanobis', 'matching', 'minkowski',
                         'rogerstanimoto', 'russellrao', 'seuclidean',
                         'sokalmichener', 'sokalsneath', 'sqeuclidean', 'yule']

    app.layout = html.Div([
        html.Div([

            html.Div([
                html.Label('Availables layouts'),
                dcc.Dropdown(
                    id='layout-update',
                    options=[{'label': i, 'value': i}
                             for i in availables_layouts],
                    value='spring_layout'
                )
            ],
                style={'width': '50%', 'display': 'inline-block'}),
            html.Div([
                html.Label('Availables metrics'),
                dcc.Dropdown(
                    id='metric-update',
                    options=[{'label': i, 'value': i}
                             for i in availables_metric],
                    value='euclidean'
                )
            ],
                style={'width': '50%', 'display': 'inline-block'}),
            html.Div([
                html.Label('Availables dintances'),
                dcc.RadioItems(
                    id='distance-update',
                    options=[{'label': i, 'value': i}
                             for i in availables_distance],
                    value='mean'
                )
            ],
                style={'width': '50%', 'display': 'inline-block'})

        ], style={
            'borderBottom': 'thin lightgrey solid',
            'backgroundColor': 'rgb(250, 250, 250)',
            'padding': '10px 5px'
        }),

        html.Div([
            dcc.Loading(
                id="loading-2", children=[dcc.Graph(
                    id='crossfilter-indicator-scatter'
                )], type="cube")

        ], style={'width': '100%', 'display': 'inline-block', 'padding': '0 20', 'min-height': '500px'}),
        html.Div([
            dcc.Loading(
                id="loading-1", children=[dcc.Graph(id='x-time-series')], type="cube"),
        ], style={'display': 'inline-block', 'width': '100%', 'min-height': '500px'})
    ])

    def network_mil(layout='spring_layout', distance='mean', metric='euclidean'):
        df_mean = pd.DataFrame()
        num_instance = {}
        for bag_id in bags_ids:
            df_points_bag = df_main.loc[df_main.bag_id == bag_id]
            df_points_bag = df_points_bag.drop(['bag_id', 'y'], axis=1)
            # mean, max, min
            if distance == 'mean':
                df_mean = df_mean.append(df_points_bag.mean(),
                                         ignore_index=True)
            elif distance == 'min':
                df_mean = df_mean.append(df_points_bag.max(),
                                         ignore_index=True)
            elif distance == 'max':
                df_mean = df_mean.append(df_points_bag.min(),
                                         ignore_index=True)

            num_instance[bag_id] = len(df_points_bag) + 10

        # create graph G
        G = nx.Graph()

        df_points_bag = df_mean
        dist = pdist(df_points_bag, metric)
        df_dist = pd.DataFrame(squareform(dist))

        nodes_inst = list(df_dist)
        nodes_inst_name = bags_ids

        weigh_edge_all = []
        first_node_all = []
        edge_node_all = []
        for ix in nodes_inst:
            weigh_edge_all += list(df_dist.iloc[ix])
            first_node_all += [nodes_inst_name[ix]] * len(nodes_inst_name)

            min_idx = df_dist.iloc[ix].idxmin()
            dist_correct = df_dist.iloc[ix].drop([min_idx])
            dist_correct_wh = dist_correct.min()
            dist_correct_idx = dist_correct.idxmin()
            edge_node_all.append(
                (nodes_inst_name[ix], nodes_inst_name[dist_correct_idx], dist_correct_wh))

        G.add_nodes_from(nodes_inst_name)
        G.add_weighted_edges_from(edge_node_all)

        # network layout
        pos = LAYAOUTS[layout](G)

        edge_trace = go.Scatter(
            x=[],
            y=[],
            line={'width': 0.5, 'color': '#888'},
            hoverinfo='none',
            mode='lines',
            showlegend=False
        )

        for edge in G.edges():
            x0, y0 = pos[edge[0]]
            x1, y1 = pos[edge[1]]
            edge_trace['x'] += tuple([x0, x1, None])
            edge_trace['y'] += tuple([y0, y1, None])

        node_trace_positive = go.Scatter(
            x=[],
            y=[],
            text=[],
            customdata=[],
            mode='markers',
            hoverinfo='text',
            marker={
                'symbol': 'circle',
                'showscale': True,
                'colorscale': 'Reds',
                'reversescale': True,
                'color': [],
                'size': [],
                'colorbar': {
                    'thickness': 15,
                    'title': {
                        'text': '   No. of positive<br>node connections',
                        'font': {
                            'color': 'rgb(165,0,38)'
                        }
                    },
                },
                'line': {'width': 3, 'color': []}
            },
            showlegend=True
        )

        node_trace_negative = go.Scatter(
            x=[],
            y=[],
            text=[],
            customdata=[],
            mode='markers',
            hoverinfo='text',
            marker={
                'symbol': 'triangle-up',
                'showscale': True,
                'colorscale': 'Blues',
                'reversescale': True,
                'color': [],
                'size': [],
                'colorbar': {
                    'thickness': 15,
                    'title': {
                        'text': '   No. of negative<br>node connections',
                        'font': {
                            'color': 'rgb(49,54,149)'
                        }
                    },
                    'x': 1.12
                },
                'line': {'width': 3, 'color': []}
            },
            showlegend=True
        )

        for node in G.nodes():  # G.nodes(), bags_ids
            x, y = pos[node]
            label_bag = df_main.loc[df_main.bag_id == node, 'y'].unique()[
                0]
            if label_bag == 'positive':
                node_trace_positive['x'] += tuple([x])
                node_trace_positive['y'] += tuple([y])
                node_trace_positive['marker']['size'] += tuple(
                    [num_instance[node]])
                node_trace_positive['marker']['line']['color'] += tuple(
                    ["rgb(165,0,38)"])
                node_trace_positive['name'] = label_bag
            else:
                node_trace_negative['x'] += tuple([x])
                node_trace_negative['y'] += tuple([y])
                node_trace_negative['marker']['size'] += tuple(
                    [num_instance[node]])
                node_trace_negative['marker']['line']['color'] += tuple(
                    ["rgb(49,54,149)"])
                node_trace_negative['name'] = label_bag

        for node, adjacencies in enumerate(G.adjacency()):
            label_bag = df_main.loc[df_main.bag_id == adjacencies[0], 'y'].unique()[
                0]
            node_info = f'Bag: {adjacencies[0]}<br>Number of connections: {str(len(adjacencies[1]))}'
            if label_bag == 'positive':
                node_trace_positive['marker']['color'] += tuple(
                    [len(adjacencies[1])])
                node_trace_positive['text'] += tuple([node_info])
                node_trace_positive['customdata'] += tuple(
                    [str(adjacencies[0])])
            else:
                node_trace_negative['marker']['color'] += tuple(
                    [len(adjacencies[1])])
                node_trace_negative['text'] += tuple([node_info])
                node_trace_negative['customdata'] += tuple(
                    [str(adjacencies[0])])

        return edge_trace, node_trace_positive, node_trace_negative

    def figure(layout='spring_layout', distance='mean', metric='euclidean'):
        edge_trace, node_trace_positive, node_trace_negative = network_mil(
            layout, distance, metric)
        figure_info = {
            "data": [edge_trace, node_trace_positive, node_trace_negative],
            "layout": go.Layout(
                titlefont={'size': 16},
                showlegend=True,
                hovermode='closest',
                margin={'b': 20, 'l': 5, 'r': 5, 't': 40},
                xaxis={'showgrid': False,
                       'zeroline': False, 'showticklabels': False},
                yaxis={'showgrid': False,
                       'zeroline': False, 'showticklabels': False},
                clickmode='event+select',
                legend=dict(
                    x=0,
                    y=1,
                    traceorder='normal',
                    font=dict(
                        family='sans-serif',
                        size=12,
                        color='#000'
                    ),
                    bgcolor='#E2E2E2',
                    bordercolor='#FFFFFF',
                    borderwidth=2
                )
            )
        }

        return figure_info

    @app.callback(
        dash.dependencies.Output('crossfilter-indicator-scatter', 'figure'),
        [dash.dependencies.Input('layout-update', 'value'),
         dash.dependencies.Input('distance-update', 'value'),
         dash.dependencies.Input('metric-update', 'value')])
    def update_graph(layout_update, distance_update, metric_update):
        return figure(
            layout=layout_update,
            distance=distance_update,
            metric=metric_update
        )

    def create_radar(dff):
        categories = list(dff.columns)
        categories = [str(c) for c in categories if c not in ['bag_id', 'y']]
        fig = make_subplots(
            rows=1,
            cols=2,
            specs=[
                [{'type': 'polar'},    {'type': 'polar'}]
            ],
            column_widths=[1200, 1200]
        )

        count = 0
        positive_bags = []
        negative_bags = []
        for i in dff.index:
            fillcolor = px.colors.colorbrewer.Paired[count]
            count += 1
            count = count if count < len(px.colors.colorbrewer.Paired) else 0
            if dff.loc[i].y == 'positive':
                positive_bags.append(dff.loc[i].bag_id)
                fig.add_trace(go.Scatterpolar(
                    r=dff.loc[i].drop(labels=['bag_id', 'y']),
                    theta=categories,
                    fill='toself',
                    fillcolor=fillcolor,
                    name=f'Instance {i}',
                    line={
                        'color': 'rgb(165,0,38)'
                    },
                    opacity=0.7,
                    mode='lines+markers',
                    hovertext=f'Instance {i} from bag {dff.loc[i].bag_id}',
                    hoverinfo='text'
                    # legendgroup='Positive'
                ), 1, 1)
            else:
                negative_bags.append(dff.loc[i].bag_id)
                fig.add_trace(go.Scatterpolar(
                    r=dff.loc[i].drop(labels=['bag_id', 'y']),
                    theta=categories,
                    fill='toself',
                    fillcolor=fillcolor,
                    name=f'Instance {i}',
                    line={
                        'color': 'rgb(49,54,149)'
                    },
                    opacity=0.7,
                    mode='lines+markers',
                    hovertext=f'Instance {i} from bag {dff.loc[i].bag_id}',
                    hoverinfo='text'
                    # legendgroup='Negative'
                ), 1, 2)

        title = f'Positive bags selected: {", ".join(set(positive_bags))} <br>Negative bags selected: {", ".join(set(negative_bags))}'
        fig.update_layout(
            title=title,
            height=1000,
            polar=dict(
                radialaxis=dict(
                    visible=True,
                    range=[0, 1]
                ),
                angularaxis=dict(
                    direction='clockwise',
                    ticks='inside'
                )
            ),
            polar2=dict(
                radialaxis=dict(
                    visible=True,
                    range=[0, 1]
                ),
                angularaxis=dict(
                    direction='clockwise',
                    ticks='inside'
                )
            ),
            showlegend=True
        )

        return fig

    @app.callback(
        dash.dependencies.Output('x-time-series', 'figure'),
        [dash.dependencies.Input('crossfilter-indicator-scatter', 'selectedData')])
    def update_y_timeseries(selectedData):
        bags_selected = []
        if selectedData:
            for point in selectedData['points']:
                bags_selected.append(point['customdata'])
        first_positive = df_t.loc[df_t.y == 'positive'].bag_id.iloc[0]
        first_negative = df_t.loc[df_t.y == 'negative'].bag_id.iloc[0]
        bags_selected = [first_positive, first_negative] if len(
            bags_selected) == 0 else bags_selected
        df_points_bag = df_t.loc[df_t.bag_id.isin(bags_selected)]
        return create_radar(df_points_bag)
