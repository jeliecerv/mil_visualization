from django.urls import path

from .views import (ClassifierView, DataBagSimplificationView,
                    DataDistributionModalView, DataReductionView,
                    DataRepresentationView, DataVisulizationDashView,
                    DownloadDatasetView, HomePageView, ReductionAlgorithmView)

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('data-representation', DataRepresentationView.as_view(),
         name='data-representation'),
    path('data-visualization-dash', DataVisulizationDashView.as_view(),
         name='data-visualization-dash'),
    path('data-reduction', DataReductionView.as_view(),
         name='data-reduction'),
    path('data-distribution', DataDistributionModalView.as_view(),
         name='data-distribution'),
    path('data-reduction-algorithm', ReductionAlgorithmView.as_view(),
         name='data-reduction-algorithm'),
    path('data-bag-simplification', DataBagSimplificationView.as_view(),
         name='data-bag-simplification'),
    path('download-dataset', DownloadDatasetView.as_view(),
         name='download-dataset'),
    path('classifier', ClassifierView.as_view(),
         name='classifier'),
]
