from django import forms
from django.core.validators import FileExtensionValidator
from django.forms import ModelForm

from .models import Reduction


class UploadFileForm(forms.Form):
    DATASETS_EXAMPLES = [
        ('', '---'),
        ('data_gauss', 'data_gauss'),
        ('custom_data_gauss', 'custom_data_gauss'),
        ('musk1_original', 'musk1_original'),
        ('musk1_scaled', 'musk1_scaled'),
        ('musk2_original', 'musk2_original'),
        ('musk2_scaled', 'musk2_scaled'),
        ('fox_original', 'fox_original'),
        ('fox_scaled', 'fox_scaled'),
        ('tiger_original', 'tiger_original'),
        ('tiger_scaled', 'tiger_scaled'),
        ('mutagenesis1_original', 'mutagenesis1_original'),
        ('mutagenesis1_scaled', 'mutagenesis1_scaled'),
        ('mutagenesis2_original', 'mutagenesis2_original'),
        ('mutagenesis2_scaled', 'mutagenesis2_scaled'),
        ('birds_BRCR_original', 'birds_BRCR_original'),
        ('birds_BRCR_scaled', 'birds_BRCR_scaled'),
        ('birds_WIWR_original', 'birds_WIWR_original'),
        ('birds_WIWR_scaled', 'birds_WIWR_scaled'),
        ('corel_african_original', 'corel_african_original'),
        ('corel_african_scaled', 'corel_african_scaled'),
        ('elephant_original', 'elephant_original'),
        ('elephant_scaled', 'elephant_scaled'),
    ]
    # title = forms.CharField(max_length=50, required=False)
    file = forms.FileField(required=False, validators=[
                           FileExtensionValidator(['csv', 'svm'])])
    datasets = forms.ChoiceField(choices=DATASETS_EXAMPLES, required=False)


class ReductionForm(ModelForm):
    ALGORITHMS = [
        ('linear', 'Linear'),
        ('poly', 'Poly'),
        ('rbf', 'RBF')
    ]
    BAG_SIMPLIFICATION = [
        ('', 'None'),
        ('kde', 'KDE'),
        ('max', 'Max'),
        ('min', 'Min'),
        ('mean', 'Mean'),
        ('max+min', 'Max-Min'),
        ('max+min+mean', 'Max-Min-Mean')
    ]
    algorithms = forms.ChoiceField(
        choices=ALGORITHMS,
        required=False,
        widget=forms.RadioSelect,
        initial='linear'
    )
    n_components = forms.IntegerField(min_value=0, initial=4)
    by_bag = forms.BooleanField(required=False)
    kde = forms.BooleanField(required=False)
    bag_simplification = forms.ChoiceField(
        choices=BAG_SIMPLIFICATION,
        required=False,
        widget=forms.RadioSelect
    )

    class Meta:
        model = Reduction
        fields = ['reduction_method']
