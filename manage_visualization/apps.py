from django.apps import AppConfig


class ManageVisualizationConfig(AppConfig):
    name = 'manage_visualization'
