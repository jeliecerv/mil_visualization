from django.contrib import admin
from .models import Algorithm, Reduction


class AlgorithmInline(admin.TabularInline):
    model = Algorithm
    prepopulated_fields = {"slug": ("algorithm_name",)}


class ReductionAdmin(admin.ModelAdmin):
    list_display = ('reduction_method', )
    inlines = [
        AlgorithmInline,
    ]


admin.site.register(Reduction, ReductionAdmin)
