import csv
import json
# import logging
import os
import random as rand
import time
import urllib
from io import BytesIO, StringIO

import numpy as np
import pandas as pd
from bokeh.embed import components
from django.conf import settings
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView, View
from django.views.generic.edit import FormView
from google.cloud import storage
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

from .classifier.simpleMIL import simpleMIL
from .dash_plot.distribution_data import distribtion_data
from .dash_plot.donut_legends import donut_chart
from .dash_plot.interaction_network_radar import network_radar
from .forms import ReductionForm, UploadFileForm
from .models import Reduction
from .reduction_method.bag_simplification import bag_simplification
from .reduction_method.fast_ica import fastICA_reduction
from .reduction_method.isomap import isomap_k
from .reduction_method.kde import kde_process
from .reduction_method.kpca import kpca_kernels
from .reduction_method.lle import locally_linear_embedding_k
from .reduction_method.mds import mds
from .reduction_method.tsne import tsne_reduction
from .utils.utils import (clear_session_vars, data_treatment,
                          generate_gauss_dataset, get_data, get_data_info,
                          load_data_mat, load_mil_data)

# logger = logging.getLogger(__name__)


class HomePageView(FormView):
    template_name = "home.html"
    form_class = UploadFileForm

    def get_success_url(self):
        return '{}?{}'.format(reverse('data-representation'), urllib.parse.urlencode(self.queryparams))

    def handle_uploaded_file(self, f):
        self.queryparams = {
            'file-mil-data': f.name
        }
        with open(f'media/upload_files/{f.name}', 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)

    def upload_blob(self, bucket_name, source_file_name, destination_blob_name):
        """Uploads a file to the bucket."""
        # bucket_name = "your-bucket-name"
        # source_file_name = "local/path/to/file"
        # destination_blob_name = "storage-object-name"

        # source_file_name.seek(0)
        # file_handle = BytesIO(source_file_name.read())

        storage_client = storage.Client()
        bucket = storage_client.bucket(bucket_name)
        blob = bucket.blob(destination_blob_name)

        blob.upload_from_file(source_file_name)

        self.queryparams = {
            'file-mil-data': destination_blob_name
        }

    def post(self, request, *args, **kwargs):
        clear_session_vars(request)
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            if len(request.FILES) > 0:
                try:
                    # self.handle_uploaded_file(request.FILES['file'])
                    self.upload_blob(
                        'mil-datasets', request.FILES['file'], request.FILES['file'].name)
                except Exception as e:
                    # logger.error(e)
                    messages.error(
                        request, 'Something went wrong when writing to the file.')
                    return self.form_invalid(form)
            elif request.GET.get('datasets') != '':
                self.queryparams = {
                    'mil-data': request.POST.get('datasets')
                }
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class DataRepresentationView(TemplateView):
    # - Adicionar nota en el front acerca de la normalizacion.
    template_name = "data_representation.html"

    def get(self, request, *args, **kwargs):
        clear_session_vars(request)
        context = self.get_context_data(**kwargs)
        if 'error' in context:
            return HttpResponseRedirect(reverse('home'))
        return self.render_to_response(context)

    def get_form(self):
        return ReductionForm()

    def make_blob_public(self, bucket_name, blob_name):
        """Makes a blob publicly accessible."""
        storage_client = storage.Client()
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(blob_name)

        blob.make_public()
        return blob.public_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        mil_data = self.request.GET.get('mil-data')
        title_dataset = mil_data
        if mil_data:
            df = load_data_mat(mil_data)
            context['mil_data'] = mil_data
        else:
            try:
                url = self.make_blob_public(
                    'mil-datasets', self.request.GET.get("file-mil-data"))
                df = load_mil_data(url)
                title_dataset = self.request.GET.get('file-mil-data')
            except Exception as e:
                # logger.error(e)
                print(e)
                messages.error(
                    self.request, 'Something went wrong when load the dataset.')
                context = {
                    'error': 'Something went wrong when load the dataset.'}
                return context

        # Normalize data
        df = data_treatment(df, range=(0.00, 1.00))

        self.request.session['df_original'] = df.to_json()
        self.request.session['title_dataset'] = title_dataset

        context['title_dataset'] = title_dataset
        columns = [{'field': f, 'title': f}
                   for f in list(df.columns)]
        columns_describe = [{'field': f, 'title': f}
                            for f in list(df.describe().columns)]
        columns_describe.insert(0, {
            'field': 'index1',
            'title': 'Describe'
        })
        context['data'] = df.to_json(orient='records')
        context['columns'] = columns
        # Add more descriptive information
        df_describe = df.describe()
        df_describe = df_describe.drop(['count'])
        df_describe['index1'] = df_describe.index
        context['data_describe'] = df_describe.to_json(orient='records')
        context['columns_describe'] = columns_describe

        context['reduction_form'] = self.get_form()
        context['max_components'] = len(columns) - 2

        data_info = get_data_info(df)
        context.update(data_info)

        return context


class DataReductionView(View):

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        return JsonResponse(context)

    def get_context_data(self, **kwargs):
        df_main_json = self.request.session.get('df_original')
        df_main = pd.DataFrame(json.loads(df_main_json))
        df_main_reduction = None
        error = False
        error_message = ''

        self.request.session['reduction_info'] = json.dumps(self.request.POST)

        try:
            if self.request.POST.get('bag_simplification') == 'kde':
                df_main, pdf, pdf2 = kde_process(df_main)
            elif self.request.POST.get('bag_simplification') != 'None' and self.request.POST.get('bag_simplification') != '':
                df_main = bag_simplification(
                    df_main, self.request.POST.get('bag_simplification'))

            if self.request.POST.get('reduction_method') == 'kpca':
                df_main_reduction = kpca_kernels(
                    df_main,
                    kernel=self.request.POST.get('algorithms'),
                    by_bag=self.request.POST.get('by_bag'),
                    n_components=int(self.request.POST.get('n_components'))
                )
            if self.request.POST.get('reduction_method') == 'fast_ica':
                df_main_reduction = fastICA_reduction(
                    df_main,
                    algorithm=self.request.POST.get('algorithms'),
                    by_bag=self.request.POST.get('by_bag'),
                    n_components=int(self.request.POST.get('n_components'))
                )
            if self.request.POST.get('reduction_method') == 'isomap':
                df_main_reduction = isomap_k(
                    df_main,
                    by_bag=self.request.POST.get('by_bag'),
                    n_components=int(self.request.POST.get('n_components'))
                )
            if self.request.POST.get('reduction_method') == 'lle':
                df_main_reduction = locally_linear_embedding_k(
                    df_main,
                    eigen_solver=self.request.POST.get('algorithms'),
                    n_components=int(self.request.POST.get('n_components'))
                )
            if self.request.POST.get('reduction_method') == 'mds':
                df_main_reduction = mds(
                    df_main,
                    by_bag=self.request.POST.get('by_bag'),
                    n_components=int(self.request.POST.get('n_components'))
                )
            if self.request.POST.get('reduction_method') == 'tsne':
                df_main_reduction = tsne_reduction(
                    df_main,
                    by_bag=self.request.POST.get('by_bag'),
                    n_components=int(self.request.POST.get('n_components'))
                )
        except Exception as e:
            # logger.error(e)
            # messages.error(
            #     self.request, 'Something went wrong when try to reduce the dataset.')
            error_message = str(e)
            error = True

        if error:
            context = {
                'error': f'Something went wrong when try to reduce the dataset, please try with other method or change the reductions options. {error_message}'
            }
        else:
            self.request.session['df_main'] = df_main_reduction.to_json()

            columns = [{'field': f, 'title': f}
                       for f in list(df_main_reduction.columns)]
            context = {
                'df_main_reduction': df_main_reduction.to_json(orient='records'),
                'columns': columns
            }
        return context


class DataVisulizationDashView(TemplateView):
    # template_name = "data_visualization_dash.html"
    template_name = "data_visualization_dash_direct.html"

    def post(self, request, *args, **kwargs):
        request.session['columns_visualize'] = request.POST.get('columns')
        return JsonResponse({
            'columns_visualize': self.request.session.get('columns_visualize')
        })

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        title_dataset = self.request.session.get('title_dataset', '')
        context['title_dataset'] = title_dataset

        df_main_json = self.request.session.get('df_original')
        bag_simplification = self.request.session.get('bag_simplification')
        if bag_simplification:
            df_main_json = bag_simplification
        if self.request.GET.get('use_reduction'):
            df_main_json = self.request.session.get('df_main', None)

        df_main = pd.DataFrame(json.loads(df_main_json))
        df_main = self.remove_columns(df_main)

        self.request.session['df_download'] = df_main.to_json()

        network_radar(df_main)

        df = pd.DataFrame(json.loads(self.request.session.get('df_original')))

        data_info = get_data_info(df)
        donut_chart(['Positive', 'Negative'], [
                    data_info['positive_bags'], data_info['negative_bags']])

        reduction_info = self.request.session.get('reduction_info')
        reduction_form = ReductionForm(initial=json.loads(reduction_info))
        context.update({
            'reduction_form': reduction_form,
            'max_components': len(df_main.columns) - 2
        })

        context.update(data_info)

        return context

    def remove_columns(self, df_main):
        columns = self.request.session.get('columns_visualize')
        if not columns:
            return df_main

        columns = columns.split(',')
        df_result = df_main.drop(columns, axis=1)
        return df_result


class DataDistributionModalView(TemplateView):
    template_name = "data_distribution.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        attr = self.request.GET.get('attr')

        df_main_json = self.request.session.get('df_original')
        df_main = pd.DataFrame(json.loads(df_main_json))

        distribtion_data(df_main, attr)

        return context


class ReductionAlgorithmView(View):

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return JsonResponse(context)

    def get_context_data(self, **kwargs):
        context = {
            'algorithms': []
        }
        reduction_method = self.request.GET.get('reduction_method', 'kpca')
        reduction = Reduction.objects.get(reduction_method=reduction_method)
        for algorithm in reduction.algorithms.all():
            context['algorithms'].append({
                'value': algorithm.slug,
                'text': algorithm.algorithm_name
            })

        return context


class DataBagSimplificationView(View):

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        return JsonResponse(context)

    def get_context_data(self, **kwargs):
        df_main_json = self.request.session.get('df_original')
        df_main = pd.DataFrame(json.loads(df_main_json))
        df_main_reduction = None
        error = False

        self.request.session['reduction_info'] = json.dumps(self.request.POST)
        columns = [{'field': f, 'title': f}
                   for f in list(df_main.columns)]

        try:
            if self.request.POST.get('bag_simplification') == 'kde':
                df_main, pdf, pdf2 = kde_process(df_main)
                self.request.session['bag_simplification'] = df_main.to_json()
            elif self.request.POST.get('bag_simplification') != '':
                df_main = bag_simplification(
                    df_main, self.request.POST.get('bag_simplification'))
                self.request.session['bag_simplification'] = df_main.to_json()
            else:
                self.request.session['bag_simplification'] = None
        except Exception as e:
            # logger.error(e)
            messages.error(
                self.request, 'Something went wrong when try to reduce the dataset.')
            error = True

        if error:
            context = {
                'bag_simplification': df_main.to_json(orient='records'),
                'columns': columns,
                'error': 'Something went wrong when try to reduce the dataset.'
            }
        else:
            context = {
                'bag_simplification': df_main.to_json(orient='records'),
                'columns': columns
            }
        return context


class DownloadDatasetView(TemplateView):
    template_name = "list_feature.html"

    def post(self, request, *args, **kwargs):
        columns_remove = request.POST.get('columns')

        df_download = self.request.session.get('df_download')
        df_main = pd.DataFrame(json.loads(df_download))

        df_main = self.remove_columns(df_main, columns_remove)
        csv_format = df_main.to_csv(index=False)

        return JsonResponse({
            'dataset_csv': csv_format
        })

    def remove_columns(self, df_main, columns_remove):
        columns = columns_remove
        if not columns:
            return df_main

        columns = columns.split(',')
        df_result = df_main.drop(columns, axis=1)
        return df_result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        df_download = self.request.session.get('df_download')
        df_main = pd.DataFrame(json.loads(df_download))

        X, y, bags = get_data(df_main)

        context.update({
            'features': list(X.columns)
        })

        return context


class ClassifierView(TemplateView):
    template_name = "list_feature.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        df_download = self.request.session.get('df_download')
        df_main = pd.DataFrame(json.loads(df_download))

        X, labels, bags = get_data(df_main)
        context.update({
            'features': list(X.columns)
        })

        return context

    def get_data_classifier(self, df_main):
        bags_data = []
        labels_data = []

        for bag_id in df_main.bag_id.unique():
            feature_data = df_main.loc[df_main.bag_id == bag_id].loc[:,
                                                                     df_main.columns.str.startswith('feature')].astype(float)
            if feature_data.empty:
                feature_data = df_main.loc[df_main.bag_id == bag_id].loc[:,
                                                                         df_main.columns.str.startswith('comp')].astype(float)
            bags_data.append(feature_data.values)

            y = -1
            if df_main.loc[df_main.bag_id == bag_id].y.unique().item() == 'positive':
                y = 1
            labels_data.append(y)

        bags_data = np.array(bags_data)
        labels_data = np.array(labels_data)

        return bags_data, labels_data

    def classifier_data(self, bags_data, labels_data):
        # Shuffle Data
        seed = 66
        bags, labels = shuffle(
            bags_data, labels_data, random_state=rand.randint(0, 100))
        train_bags, test_bags, train_labels, test_labels = train_test_split(
            bags_data, labels_data, test_size=0.1, random_state=seed)

        accuracy_extreme, performance_extreme = self.classifier_data_type(
            train_bags, test_bags, train_labels, test_labels, 'extreme')
        accuracy_average, performance_average = self.classifier_data_type(
            train_bags, test_bags, train_labels, test_labels, 'average')
        accuracy_max, performance_max = self.classifier_data_type(
            train_bags, test_bags, train_labels, test_labels, 'max')
        accuracy_min, performance_min = self.classifier_data_type(
            train_bags, test_bags, train_labels, test_labels, 'min')

        accuracy = {
            'accuracy_extreme': accuracy_extreme,
            'accuracy_average': accuracy_average,
            'accuracy_max': accuracy_max,
            'accuracy_min': accuracy_min,
            'performance_extreme': performance_extreme,
            'performance_average': performance_average,
            'performance_max': performance_max,
            'performance_min': performance_min
        }

        return accuracy

    def classifier_data_type(self, train_bags, test_bags, train_labels, test_labels, type_classifier='extreme'):
        start = time.time()

        simple_mil = simpleMIL()
        simple_mil.fit(train_bags, train_labels, type=type_classifier)
        predictions, accuracy = simple_mil.predict(test_bags, test_labels)

        end = time.time()
        performance = round(end - start, 4)

        return accuracy * 100, performance

    def post(self, request, *args, **kwargs):
        columns_remove = request.POST.get('columns')

        df_download = self.request.session.get('df_download')
        df_main = pd.DataFrame(json.loads(df_download))

        df_main = self.remove_columns(df_main, columns_remove)

        bags_data, labels_data = self.get_data_classifier(df_main)
        try:
            response = self.classifier_data(bags_data, labels_data)
        except Exception as e:
            # logger.error(e)
            # messages.error(
            #     self.request, 'Something went wrong when try to classify the dataset.')
            response = {
                'error': f'Something went wrong when try to classify the dataset'
            }

        return JsonResponse(response)

    def remove_columns(self, df_main, columns_remove):
        columns = columns_remove
        if not columns:
            return df_main

        columns = columns.split(',')
        df_result = df_main.drop(columns, axis=1)
        return df_result
