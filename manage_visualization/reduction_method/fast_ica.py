"""
http://theautomatic.net/2018/06/23/ica-on-images-with-python/
"""
import pandas as pd
import numpy as np
from sklearn.decomposition import FastICA

from manage_visualization.utils.utils import get_data, get_data_by_bag


def fastICA_reduction(df, by_bag=False, algorithm='parallel', n_components=2):
    X, y, bags = get_data(df)
    if by_bag:
        number_bags = len(df.bag_id.unique())

    y_clone = y.copy()
    bags_clone = bags.copy()

    fast_ica = FastICA(n_components=n_components, algorithm=algorithm)

    y_aux = y_clone.copy()
    bags_aux = bags_clone.copy()
    if by_bag:
        X_r = None
        for j in pd.unique(bags_clone):
            x, y_ = get_data_by_bag(df, j)
            fast_ica.fit(x)
            X_r_aux = fast_ica.transform(x)
            if X_r is None and not np.isnan(X_r_aux).any() and len(X_r_aux[0]) >= n_components:
                X_r = X_r_aux
            elif not np.isnan(X_r_aux).any() and len(X_r_aux[0]) >= n_components:
                X_r = np.concatenate((X_r, X_r_aux), axis=0)
            else:
                y_aux = y_aux.drop(x.index)
                bags_aux = bags_aux.drop(x.index)
    else:
        X_r = fast_ica.fit_transform(X)

    y = y_aux
    bags = bags_aux

    columns_name = [f'comp-{i}' for i in range(n_components)]
    df_result = pd.concat((bags.reset_index(drop=True),
                           y.reset_index(drop=True),
                           pd.DataFrame(X_r, columns=columns_name).reset_index(drop=True)), axis=1)

    return df_result
