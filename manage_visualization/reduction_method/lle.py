import numpy as np
import pandas as pd
from sklearn import datasets, manifold

from manage_visualization.utils.utils import get_data


def locally_linear_embedding_k(df, n_components=2, n_neighbors=1, eigen_solver='dense'):
    '''
    eigen_solver : string, {‘auto’, ‘arpack’, ‘dense’}
    '''
    X, y, bags = get_data(df)
    # Ks = [1, 5, 25, y.size-1]
    # for i, k in enumerate(Ks):
    lle = manifold.LocallyLinearEmbedding(
        n_components=n_components,
        n_neighbors=n_neighbors,
        eigen_solver=eigen_solver
    )
    X_r = lle.fit_transform(X)

    columns_name = [f'comp-{i}' for i in range(n_components)]
    df_result = pd.concat((bags.reset_index(drop=True),
                           y.reset_index(drop=True),
                           pd.DataFrame(X_r, columns=columns_name).reset_index(drop=True)), axis=1)

    return df_result
