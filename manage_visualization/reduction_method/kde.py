# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KernelDensity

from manage_visualization.utils.utils import (get_data, get_data_by_bag,
                                              get_negative_info)


def kde_process(df):
    X_negative, y_negative, bags_negative = get_negative_info(df)
    X, y, bags = get_data(df)
    params = {'bandwidth': np.logspace(-1, 1, 30)}
    grid = GridSearchCV(KernelDensity(), params, cv=5)
    grid.fit(X_negative)

    kde = grid.best_estimator_

    new_bag = None
    new_X = None
    new_y = None

    X_max = None
    X_min = None

    for i in np.unique(bags):
        # Encuentre las instancias de cada bolsa
        indices = np.where(bags == i)[0]
        x, y_bag = get_data_by_bag(df, i)

        # Calcule la pdf para las instancias de la bolsa
        pdf = kde.score_samples(x)

        # Calcule el max y el min y encuentre sus índices
        max_pdf = np.max(pdf)
        min_pdf = np.min(pdf)
        iMax = str(indices[np.where(pdf == max_pdf)[0][0]])
        iMin = str(indices[np.where(pdf == min_pdf)[0][0]])

        # Agrege a la bolsa solo las instancias con el max y el min
        if new_bag is None:
            new_bag = np.vstack([np.full((2, 1), i)])
        else:
            new_bag = np.vstack([new_bag, np.full((2, 1), i)])

        if new_y is None:
            new_y = np.vstack([np.full((2, 1), y_bag.iloc(0)[0])])
        else:
            new_y = np.vstack([new_y, np.full((2, 1), y_bag.iloc(0)[0])])

        # Agregue a la matriz de datos el max y el min
        if new_X is None:
            new_X = np.vstack([x.loc[iMax, :]])
            new_X = np.vstack([new_X, x.loc[iMin, :]])
        else:
            new_X = np.vstack([new_X, x.loc[iMax, :]])
            new_X = np.vstack([new_X, x.loc[iMin, :]])

        # Para visualizar separa max de min
        if X_max is None:
            X_max = np.vstack([x.loc[iMax, :]])
        else:
            X_max = np.vstack([X_max, x.loc[iMax, :]])

        if X_min is None:
            X_min = np.vstack([x.loc[iMin, :]])
        else:
            X_min = np.vstack([X_min, x.loc[iMin, :]])

    pdf = kde.score_samples(X_max)
    pdf2 = kde.score_samples(X_min)

    size = len(new_X[0])
    columns_name = ['feature-%s' % x for x in range(0, size)]
    columns_name.insert(0, 'y')
    columns_name.insert(0, 'bag_id')
    new_df = pd.DataFrame(np.concatenate((new_bag, new_y, new_X), axis=1),
                          columns=columns_name)

    return new_df, pdf, pdf2
