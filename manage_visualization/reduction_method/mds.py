import numpy as np
import pandas as pd
from sklearn import datasets, manifold

from manage_visualization.utils.utils import get_data, get_data_by_bag


def mds(df, by_bag=False, n_components=2):
    X, y, bags = get_data(df)
    if by_bag:
        number_bags = len(df.bag_id.unique())

    if by_bag:
        X_r = None
        for j in range(number_bags):
            x, y_ = get_data_by_bag(df, j)
            mds = manifold.MDS(n_components=n_components)
            X_r_aux = mds.fit_transform(x)
            if X_r is None:
                X_r = X_r_aux
            else:
                X_r = np.concatenate((X_r, X_r_aux), axis=0)
    else:
        mds = manifold.MDS(n_components=n_components)
        X_r = mds.fit_transform(X)

    columns_name = [f'comp-{i}' for i in range(n_components)]
    df_result = pd.concat((bags.reset_index(drop=True),
                           y.reset_index(drop=True),
                           pd.DataFrame(X_r, columns=columns_name).reset_index(drop=True)), axis=1)

    return df_result
