# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

from manage_visualization.utils.utils import get_data, get_data_by_bag


def bag_simplification(df, filters='min+max+mean'):
    """
    min_value = X[column].min()
    max_value = X[column].max()
    mean_value = X[column].mean()
    """
    filters_split = filters.split('+')
    df_result = pd.DataFrame()
    for bag in df.bag_id.unique():
        bag_instances = df[df.bag_id == bag]
        X, y, bags = get_data(bag_instances)
        df_result_bag = pd.DataFrame()
        for column in X.columns:
            values = []
            for filter in filters_split:
                values.append(getattr(X[column], filter)())
            df_result_bag[column] = values
        df_result_bag['y'] = [y.unique()[0]] * len(filters_split)
        df_result_bag['bag_id'] = [bag] * len(filters_split)
        df_result = df_result.append(df_result_bag, ignore_index=True)

    return df_result
