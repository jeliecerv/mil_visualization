import numpy as np
import pandas as pd
from sklearn import manifold
from manage_visualization.utils.utils import get_data, get_data_by_bag


def isomap_k(df, by_bag=False, n_components=2, n_neighbors=1):
    '''
    test the performance with different n_neighbors and reduce to n_components-D
    '''
    X, y, bags = get_data(df)
    if by_bag:
        number_bags = len(df.bag_id.unique())

    y_clone = y.copy()
    bags_clone = bags.copy()

    y_aux = y_clone.copy()
    bags_aux = bags_clone.copy()
    if by_bag:
        y_clone = y.copy()

        X_r = None
        for j in range(number_bags):
            x, y_ = get_data_by_bag(df, j)
            isomap = manifold.Isomap(
                n_components=n_components, n_neighbors=n_neighbors)
            X_r_aux = isomap.fit_transform(x)

            if X_r is None and not np.isnan(X_r_aux).any() and len(X_r_aux[0]) >= n_components:
                X_r = X_r_aux
            elif not np.isnan(X_r_aux).any() and len(X_r_aux[0]) >= n_components:
                X_r = np.concatenate((X_r, X_r_aux), axis=0)
            else:
                y_aux = y_aux.drop(x.index)
                bags_aux = bags_aux.drop(x.index)
    else:
        isomap = manifold.Isomap(
            n_components=n_components, n_neighbors=n_neighbors)
        X_r = isomap.fit_transform(X)

    y = y_aux
    bags = bags_aux

    columns_name = [f'comp-{i}' for i in range(n_components)]
    df_result = pd.concat((bags.reset_index(drop=True),
                           y.reset_index(drop=True),
                           pd.DataFrame(X_r, columns=columns_name).reset_index(drop=True)), axis=1)

    return df_result
