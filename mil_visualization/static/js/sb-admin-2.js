(function ($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  jQuery("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function () {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function () {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function (e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });

  // Radio Buttons
  $('.radio-btn').on('click', 'a', function () {
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#' + tog).prop('value', sel);
    $('#' + tog).trigger("change");

    $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
  })

  // Slider
  $('#id_n_components').slider({
    formatter: function (value) {
      return 'Current value: ' + value;
    }
  });

  $('#id_n_components').on("slide", function (slideEvt) {
    $("#id_n_componentsSliderVal").text(slideEvt.value);
  });

  // Ajax
  $("#id_reduction_method").change(function () {
    var url = $("#urlReductionAlgorithm").val();
    var data = {
      'reduction_method': $(this).val()
    };
    $.ajax({
      method: "GET",
      url: url,
      data: data,
      success: handleFormSuccessAlgorithm,
      error: handleFormError,
    });
  });

  function handleFormSuccessAlgorithm(data, textStatus, jqXHR) {
    console.log(data);
    console.log(textStatus);
    $("#radioBtnAlgorithm").empty();
    var selected = $("#algorithms").val();
    var disabled = $("#algorithms").hasClass("disabled");
    $.each(data['algorithms'], function (i, val) {
      var is_active = "notActive";
      var classDisabled = "";
      if (selected == val['value'] && disabled) {
        is_active = "active";
      }
      if (disabled) {
        classDisabled = "disabled";
      }
      var radioBtn = $('<a class="btn btn-primary btn-sm ' + is_active + ' ' + classDisabled + '" data-toggle="algorithms" data-title="' + val['value'] + '">' + val['text'] + '</a>');
      radioBtn.appendTo('#radioBtnAlgorithm');
    });
    if (data['algorithms'].length <= 0) {
      $("#algorithms_reduction").hide();
    } else {
      $("#algorithms_reduction").show();
    }
  };

  $('[data-toggle="tooltip"]').tooltip();

})(jQuery); // End of use strict

function handleFormError(jqXHR, textStatus, errorThrown) {
  console.log(jqXHR)
  console.log(textStatus)
  console.log(errorThrown)

  $(".top-message-error").clone().addClass('top-message-error-clone').appendTo(".top-messages");
  $(".top-messages").show('fast');
  $(".top-message-error-clone").show('fast');
  $(".top-message-error-clone .alert-error").prepend("Something went wrong when try to reduce the dataset, please try with other method or change the reductions options.");
  $(document).scrollTop(0);
  return;
};

function handleComplete(jqXHR, textStatus, errorThrown) {
  $('.loader-backdrop').removeClass('show').addClass('no-show');
};

function handleBeforeSend(jqXHR, textStatus, errorThrown) {
  $('.loader-backdrop').removeClass('no-show').addClass('show');
}